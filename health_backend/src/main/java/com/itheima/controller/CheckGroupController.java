package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckGroup;
import com.itheima.service.CheckGroupService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/checkGroup")
public class CheckGroupController {

	@Reference
	private CheckGroupService checkGroupService;
	@RequestMapping("/add")
	public Result add(Integer[] checkitemIds,@RequestBody CheckGroup checkGroup) {
		try {
			checkGroupService.add(checkitemIds, checkGroup);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.ADD_CHECKGROUP_FAIL);
		}
		return new Result(true, MessageConstant.ADD_CHECKGROUP_SUCCESS);
	}

	@RequestMapping("/findPage")
	public Result findPage(@RequestBody QueryPageBean queryPageBean) {
		try {
			PageResult pageResult= checkGroupService.findPage(queryPageBean);
			return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS, pageResult);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.QUERY_CHECKGROUP_FAIL);
		}

	}

	@RequestMapping("/findById")
	public Result findById(Integer id) {
		try {
			CheckGroup checkGroup = checkGroupService.findById(id);
			return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS, checkGroup);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.QUERY_CHECKGROUP_FAIL);
		}
	}

	@RequestMapping("/findCheckItemIdsByCheckGroupId")
	public Result findCheckItemIdsByCheckGroupId(Integer id) {
		try {
			List<Integer>  checkitemIds= checkGroupService.findCheckItemIdsByCheckGroupId(id);

			return new Result(true, MessageConstant.QUERY_CHECKITEM_SUCCESS, checkitemIds);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.QUERY_CHECKITEM_FAIL);
		}
	}

	@RequestMapping("/edit")
	public Result edit(Integer[] checkitemIds,@RequestBody CheckGroup checkGroup) {
		try {
			checkGroupService.edit(checkitemIds, checkGroup);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.EDIT_CHECKGROUP_FAIL);
		}
		return new Result(true, MessageConstant.EDIT_CHECKGROUP_SUCCESS);
	}
	@RequestMapping("/delete")
	public Result delete(Integer id) {
		try {
			checkGroupService.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.DELETE_CHECKGROUP_FAIL);
		}
		return new Result(true, MessageConstant.DELETE_CHECKGROUP_SUCCESS);
	}

	@RequestMapping("/findAll")
	public Result findAll() {
		try {
			List<CheckGroup> list = checkGroupService.findAll();
			return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS, list);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.QUERY_CHECKGROUP_FAIL);
		}
	}


}
