package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.Result;
import com.itheima.service.MemberService;
import com.itheima.service.ReportService;
import com.itheima.service.SetmealService;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/report")
public class ReportController {
	@Reference
	private MemberService memberService;

	@Reference
	private SetmealService setmealService;

	@Reference
	private ReportService reportService;
	//获取会员信息----report_member.html数据回显
	@RequestMapping("/getMemberReport")
	public Result getMemberReport() {
		Map<String, Object> map = new HashMap<>();
		List<String> months = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -12);
		for (int i = 0; i < 12; i++) {
			calendar.add(Calendar.MONTH, 1);
			Date date = calendar.getTime();
			months.add(new SimpleDateFormat("yyyy.MM").format(date));
		}
		map.put("months", months);
		List<Integer> memberCount = memberService.findMemberCountByMonths(months);
		map.put("memberCount", memberCount);
		return new Result(true, MessageConstant.GET_MEMBER_NUMBER_REPORT_SUCCESS, map);
	}
	//套餐预约占比饼状图  数据
	@RequestMapping("/getSetmealReport")
	public Result getSetmealReport() {
		Map<String, Object> data = new HashMap<>();

		List<Map<String, Object>> setmealCount = setmealService.findSetmealCount();
		data.put("setmealCount", setmealCount);

		List<String> setmealNames = new ArrayList<>();
		for (Map<String, Object> map : setmealCount) {
			String name = (String) map.get("name");
			setmealNames.add(name);
		}
		data.put("setmealNames", setmealNames);


		return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS, data);
	}


	//运营数据统计----  模拟静态数据
	@RequestMapping("/getBusinessReportDataStatic")
	public Result getBusinessReportDataStatic() {

		//创建map集合存储对象
		Map<String, Object> data = new HashMap<>();
		data.put("reportDate", new Date());
		data.put("todayNewMember", 10);
		data.put("totalMember", 12);
		data.put("thisWeekNewMember", 13);
		data.put("thisMonthNewMember", 14);
		data.put("todayOrderNumber", 15);
		data.put("todayVisitsNumber", 16);
		data.put("thisWeekOrderNumber", 17);
		data.put("thisWeekVisitsNumber", 18);
		data.put("thisMonthOrderNumber", 19);
		data.put("thisMonthVisitsNumber", 20);


		List<Map<String, Object>> hotSetmeal = new ArrayList<>();

		Map<String, Object> map1 = new HashMap<>();
		Map<String, Object> map2 = new HashMap<>();

		map1.put("name", "阳光爸妈升级肿瘤12项筛查（男女单人）体检套餐");
		map1.put("setmeal_count", 200);
		map1.put("proportion", 0.222);

		map2.put("name", "'阳光爸妈升级肿瘤12项筛查体检套餐'");
		map2.put("setmeal_count",200);
		map2.put("proportion",0.222);

		hotSetmeal.add(map1);
		hotSetmeal.add(map2);
		data.put("hotsetmeal",hotSetmeal);

		return new Result(true, MessageConstant.GET_BUSINESS_REPORT_SUCCESS, data);
	}
	//运营数据 ---动态获取
	@RequestMapping("/getBusinessReportData")
	public Result getBusinessReportData() {

		Map<String, Object> data = null;
		try {
			data = reportService.getBusinessReportData();
			return new Result(true, MessageConstant.GET_BUSINESS_REPORT_SUCCESS, data);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.GET_BUSINESS_REPORT_SUCCESS, data);
		}
	}

	//导出文件 excel
	@RequestMapping("/exportBusinessReport")
	public Result exportBusinessReport(HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String, Object> result = reportService.getBusinessReportData();

			//取出返回结果数据，准备将报表数据写入到Excel文件中
			String reportDate = (String) result.get("reportDate");
			Integer todayNewMember = (Integer) result.get("todayNewMember");
			Integer totalMember = (Integer) result.get("totalMember");
			Integer thisWeekNewMember = (Integer) result.get("thisWeekNewMember");
			Integer thisMonthNewMember = (Integer) result.get("thisMonthNewMember");
			Integer todayOrderNumber = (Integer) result.get("todayOrderNumber");
			Integer thisWeekOrderNumber = (Integer) result.get("thisWeekOrderNumber");
			Integer thisMonthOrderNumber = (Integer) result.get("thisMonthOrderNumber");
			Integer todayVisitsNumber = (Integer) result.get("todayVisitsNumber");
			Integer thisWeekVisitsNumber = (Integer) result.get("thisWeekVisitsNumber");
			Integer thisMonthVisitsNumber = (Integer) result.get("thisMonthVisitsNumber");
			List<Map> hotSetmeal = (List<Map>) result.get("hotSetmeal");

			//获取模板excel文件的路径
			String filePath = request.getSession().getServletContext().getRealPath("template" + File.separator + "report_template.xlsx");
			//基于模板,在内存中创建一个excel对象
			XSSFWorkbook excel = new XSSFWorkbook(new FileInputStream(new File(filePath)));
			//读取第一个工作表
			XSSFSheet sheet = excel.getSheetAt(0);

			//获取第三行 2
			XSSFRow row2 = sheet.getRow(2);
			row2.getCell(5).setCellValue(reportDate);

			XSSFRow row4 = sheet.getRow(4);
			row4.getCell(5).setCellValue(todayNewMember);
			row4.getCell(7).setCellValue(totalMember);

			XSSFRow row5 = sheet.getRow(5);
			row5.getCell(5).setCellValue(thisWeekNewMember);
			row5.getCell(7).setCellValue(thisMonthNewMember);

			XSSFRow row7 = sheet.getRow(7);
			row7.getCell(5).setCellValue(todayOrderNumber);
			row7.getCell(7).setCellValue(thisWeekOrderNumber);

			XSSFRow row8 = sheet.getRow(8);
			row8.getCell(5).setCellValue(thisMonthOrderNumber);
			row8.getCell(7).setCellValue(todayVisitsNumber);

			XSSFRow row9 = sheet.getRow(9);
			row9.getCell(5).setCellValue(thisWeekVisitsNumber);
			row9.getCell(7).setCellValue(thisMonthVisitsNumber);

			XSSFRow row = null;
			int rowNum = 12;
			for (Map map : hotSetmeal) {
				String name = (String) map.get("name");
				long setmeal_count = (long) map.get("setmeal_count");
				BigDecimal proportion = (BigDecimal) map.get("proportion");
				row = sheet.getRow(rowNum++);
				row.getCell(4).setCellValue(name);
				row.getCell(5).setCellValue(setmeal_count);
				row.getCell(6).setCellValue(proportion.doubleValue());

			}
			//使用输出流进行表格下载,基于浏览器作为客户端下载
			OutputStream out = response.getOutputStream();
			response.setContentType("application/vnd.ms-excel");//代表的是Excel文件类型
			response.setHeader("content-Disposition", "attachment;filename=report.xlsx");//指定以附件形式进行下载
			excel.write(out);


			out.flush();
			out.close();
			excel.close();

			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.GET_BUSINESS_REPORT_FAIL);
		}
	}

	//导出pdf
	@RequestMapping("/exportBusinessReport_PDF")
	public Result exportBusinessReport_PDF(HttpServletResponse response) {
		try {
			//从数据库中读取数据
			Map<String, Object> data = reportService.getBusinessReportData();
			List<Map> hotSetmeal = (List<Map>) data.get("hotSetmeal");

			//模板文件路径
			String jrxmlPath = "E:\\project\\health_1\\health_parent\\health_backend\\src\\main\\resources\\health_business3.jrxml";
			//目标文件路径
			String jasperPath = "E:\\project\\health_1\\health_parent\\health_backend\\src\\main\\resources\\health_business3.jasper";
			//编译
			JasperCompileManager.compileReportToFile(jrxmlPath, jasperPath);
			//数据注入
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperPath, data, new JRBeanCollectionDataSource(hotSetmeal));
			//响应下载
			//使用输出流进行表格下载,基于浏览器作为客户端下载
			ServletOutputStream out = response.getOutputStream();
			response.setContentType("application/pdf");
			response.setHeader("content-Disposition", "attachment;filename=report.pdf");

			JasperExportManager.exportReportToPdfStream(jasperPrint, out);

			out.flush();
			out.close();

			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.GET_BUSINESS_REPORT_FAIL);
		}

	}

}
