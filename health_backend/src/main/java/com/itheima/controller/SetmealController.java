package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.constant.RedisConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Setmeal;
import com.itheima.service.SetmealService;
import com.itheima.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.JedisPool;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/setmeal")
public class SetmealController {
	@Autowired
	private JedisPool jedisPool;
	@RequestMapping("/upload")
	public Result upload(@RequestParam("imgFile") MultipartFile imgFile) {
		//获取文件后缀
		String originalFilename = imgFile.getOriginalFilename();//获取文件名
		int index = originalFilename.lastIndexOf(".");//最后一个 . 的索引
		String extention = originalFilename.substring(index - 1); //获取后缀
		//获取
		String fileName = UUID.randomUUID().toString() + extention;
		try {
			QiniuUtils.upload2Qiniu(imgFile.getBytes(), fileName);

			//将上传的文件名保存到redis中
			jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_RESOURCES, fileName);

		} catch (IOException e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.PIC_UPLOAD_FAIL);
		}
		return new Result(true, MessageConstant.PIC_UPLOAD_SUCCESS, fileName);
	}

	@Reference
	private SetmealService setmealService;
	@RequestMapping("/add")
	public Result add(@RequestBody Setmeal setmeal, Integer[] checkgroupIds) {
		try {
			setmealService.add(setmeal, checkgroupIds);
			return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.QUERY_SETMEAL_FAIL);
		}
	}

	@RequestMapping("/findpage")
	public Result findpage(@RequestBody QueryPageBean queryPageBean) {
		try {
			PageResult pageResult = setmealService.pageQuery(queryPageBean);

			return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS, pageResult);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.QUERY_SETMEAL_FAIL);
		}
	}

	@RequestMapping("delete")
	public Result delete(Integer id){
		try {
			setmealService.delete(id);
			return new Result(true, "删除套餐成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除套餐失败");
		}
	}

	//根据id查询---回显数据
	@RequestMapping("/findById")
	public Result findById(Integer id) {
		try {
			Setmeal setmeal = setmealService.findById(id);
			return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS, setmeal);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.QUERY_SETMEALLIST_FAIL);
		}
	}
	//根据体检套餐的id查询检查组的id ----回显数据--体检套餐的检查项勾选
	@RequestMapping("/findCheckGroupIdsBySetmealId")
	public Result findCheckGroupIdsBySetmealId(Integer id) {
		try {
			List<Integer> list = setmealService.findCheckGroupIdsBySetmealId(id);
			return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS, list);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Result(false, MessageConstant.QUERY_SETMEAL_FAIL);
	}


	//编辑
	@RequestMapping("/edit")
	public Result edit(Integer[]  checkgroupIds, @RequestBody Setmeal setmeal) {

		try {
			setmealService.edit(checkgroupIds, setmeal);
			return new Result(true, "编辑体检套餐成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "编辑体检套餐失败");
		}


	}


}
