package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.Result;

import com.itheima.service.UserService;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/user")
public class UserController {
	@Reference
	private UserService userService;

	@RequestMapping("/getUsername")
	public Result getUsername() {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (user != null) {
			return new Result(true, MessageConstant.GET_USERNAME_SUCCESS, user.getUsername());
		}
		return new Result(false, MessageConstant.GET_MENU_FAIL);
	}

	@RequestMapping("/getUsername2")
	public Result getUsername2() {
		Map map = new HashMap();
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (user != null) {
			map.put("username", user.getUsername());
			Set<String> auths = AuthorityUtils.authorityListToSet(user.getAuthorities());
			map.put("auths", auths);
			return new Result(true, MessageConstant.GET_USERNAME_SUCCESS, map);
		}
		return new Result(false, MessageConstant.GET_USERNAME_FAIL);


	}
}
