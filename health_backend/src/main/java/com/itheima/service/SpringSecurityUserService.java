package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.pojo.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * springSecurity安全框架
 */
@Component
public class SpringSecurityUserService implements UserDetailsService {

	@Reference//远程调用UserService
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		//调用service层,根据用户填写的用户名到数据库中查找用户信息,判断是否存在,若为空,返回null,若不为空,对比密码
		User user = userService.findByUsername(username);
		if (user == null) {
			return null;
		}
		//遍历user,获取角色信息
		List<GrantedAuthority> list = new ArrayList<>();
		Set<Role> roles = user.getRoles();
		for (Role role : roles) {
			list.add(new SimpleGrantedAuthority(role.getKeyword()));
			//获取单个角色中的权限,用Set接收,类型为Permission
			Set<Permission> permissions = role.getPermissions();
			for (Permission permission : permissions) {
				list.add(new SimpleGrantedAuthority(permission.getKeyword()));
			}
		}
		//创建user对象
		org.springframework.security.core.userdetails.User securityUser =
				new org.springframework.security.core.userdetails.User(username, user.getPassword(), list);
		return securityUser;
	}
}
