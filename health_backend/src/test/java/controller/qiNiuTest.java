package controller;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.junit.Test;

public class qiNiuTest {
	//测试 --
	@Test
	public void test01() throws Exception {
		//构造一个带指定 Region 对象的配置类
		Configuration cfg = new Configuration(Zone.zone0());
//...其他参数参考类注释
		UploadManager uploadManager = new UploadManager(cfg);
//...生成上传凭证，然后准备上传
		String accessKey = "V4etNPAlsIyhfJ9h4P5wNFIzPELBw6pB_muR1kpg";
		String secretKey = "9dcLQBMz7xpVtoWXDgXphjQH25yXcEa9e7KRtKaB";
		String bucket = "kongyi-health-2";
//如果是Windows情况下，格式是 D:\\qiniu\\test.png
		String localFilePath = "D:\\电脑\\Pictures\\芬兰北极光2.jpg";
//默认不指定key的情况下，以文件内容的hash值作为文件名
		String key = null;
		Auth auth = Auth.create(accessKey, secretKey);
		String upToken = auth.uploadToken(bucket);
		try {
			Response response = uploadManager.put(localFilePath, key, upToken);
			//解析上传成功的结果
			DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
			System.out.println(putRet.key);
			System.out.println(putRet.hash);
		} catch (QiniuException ex) {
			Response r = ex.response;
			System.err.println(r.toString());
			try {
				System.err.println(r.bodyString());
			} catch (QiniuException ex2) {
				//ignore
			}
		}
	}
}
