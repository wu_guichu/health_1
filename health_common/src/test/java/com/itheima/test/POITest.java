package com.itheima.test;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class POITest {
	//测试 --
	@Test
	public void test01() throws Exception {
		//创建workbook对象
		Workbook excel = new XSSFWorkbook(new FileInputStream(new File("C:\\Users\\ME\\Desktop\\解决bug.xlsx")));
		// 获取sheet
		Sheet sheet = excel.getSheetAt(0);

		System.out.println(" = = = = = = = FirstCellNum : " + sheet.getFirstRowNum());
		System.out.println(" = = = = = = = LastCellNum : " +sheet.getLastRowNum());

		// 遍历,获取row
		for (Row row : sheet) {

			//获取单元格
			for (Cell cell : row) {
				//获取单元格内数据----这是String类型
				System.out.println(cell.getStringCellValue());
			}

		}

		excel.close();
	}

	//测试 -- POI 读取excel文件
	@Test
	public void test02() throws Exception {
		Workbook workbook = new XSSFWorkbook(new FileInputStream(new File("C:\\Users\\ME\\Desktop\\解决bug.xlsx")));
		Sheet sheet = workbook.getSheetAt(0);
		int firstRowNum = sheet.getFirstRowNum();
		int lastRowNum = sheet.getLastRowNum();
		System.out.println("firstRowNum  = " + firstRowNum);
		System.out.println("lastRowNum  = " + lastRowNum);

		for (int i = firstRowNum; i <= lastRowNum; i++) {
			Row row = sheet.getRow(i);
			String data = "";
			short firstCellNum = row.getFirstCellNum();
			short lastCellNum = row.getLastCellNum();
			for (int j = 0; j < lastCellNum; j++) {
				data += row.getCell(j) + "\t\t";

			}
			System.out.println(data);
		}
		workbook.close();
	}


	//测试 -- POI写入Excel

	/**
	 * 创建workbook
	 * 创建sheet
	 * 创建row
	 * 创建cell
	 * @throws Exception
	 */
	@Test
	public void test03() throws Exception {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("sheet_1");

		Row row0 = sheet.createRow(0);
		row0.createCell(1).setCellValue("姓名");
		row0.createCell(2).setCellValue("性别");
		row0.createCell(3).setCellValue("年龄");
		row0.createCell(4).setCellValue("住址");
		Row row1 = sheet.createRow(1);
		row1.createCell(1).setCellValue("小明");
		row1.createCell(2).setCellValue("男");
		row1.createCell(3).setCellValue("24");
		row1.createCell(4).setCellValue("北京");
		FileOutputStream file = new FileOutputStream(new File("D:\\workbook_1.xlsx"));
		workbook.write(file);
		file.flush();
		file.close();

	}
}



