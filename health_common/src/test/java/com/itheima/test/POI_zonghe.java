package com.itheima.test;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static org.apache.poi.ss.usermodel.Cell.*;

/**
 * POI综合练习  读取excel文档并输出
 * 考虑两种格式  .xls 和 .xlsx
 *
 */

public class POI_zonghe {
	public static void main(String[] args) {
		String fileName = "D:\\JAVA\\黑马程序员培班\\就业班\\每一天\\2.线下\\6.项目一-传智健康\\day05-POI&\\传智健康-Day05\\我的报表.xls";
		System.out.println(fileName);

		Workbook workbook = null;
		if (fileName.endsWith(".xls")) {
			try {
				workbook = new HSSFWorkbook(new FileInputStream(new File(fileName)));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (fileName.endsWith(".xlsx")) {
			try {
				workbook = new XSSFWorkbook(new FileInputStream(new File(fileName)));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("无法识别的文件类型");
		}
		Sheet sheet = workbook.getSheetAt(0);
		int lastRowNum = sheet.getLastRowNum();
		for (int i = 0; i <= lastRowNum; i++) {
			Row row = sheet.getRow(i);

			short lastCellNum = row.getLastCellNum();
			String param = "";
			for (int j = 0; j < lastCellNum; j++) {
				Cell cell = row.getCell(j);
				int cellType = cell.getCellType();
				switch (cellType) {
					case CELL_TYPE_BLANK:
						param += "" + "\t";
						break;
					case CELL_TYPE_BOOLEAN:
						param += cell.getBooleanCellValue() + "\t";
						break;
					case CELL_TYPE_ERROR:
						param += cell.getErrorCellValue() + "\t";
						break;
					case CELL_TYPE_FORMULA:
						param += cell.getNumericCellValue() + "\t";
						break;
					case CELL_TYPE_NUMERIC:
						param += cell.getNumericCellValue() + "\t";
						break;
					case CELL_TYPE_STRING:
						param += cell.getStringCellValue() + "\t";
						break;
					default:
						break;
				}
			}
			System.out.println(param);
		}

		try {
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
