package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetmealService {

	public void add(Setmeal setmeal, Integer[] checkgroupIds);

	PageResult pageQuery(QueryPageBean queryPageBean);

	List<Setmeal> findAll();

	Setmeal findById(Integer id);

	void delete(Integer id);

	List<Integer> findCheckGroupIdsBySetmealId(Integer id);
	//编辑
	void edit(Integer[] checkgroupIds, Setmeal setmeal);
	//饼图--查询体检套餐订单
	List<Map<String, Object>> findSetmealCount();
}
