package com.itheima.jobs;

import com.itheima.constant.RedisConstant;
import com.itheima.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.JedisPool;

import java.util.Date;
import java.util.Set;

public class ClearImgJob {
	@Autowired
	private JedisPool jedisPool;
	//删除垃圾图片
	public void clearImg() {
		Set<String> set = jedisPool.getResource().sdiff(RedisConstant.SETMEAL_PIC_RESOURCES, RedisConstant.SETMEAL_PIC_DB_RESOURCES);
		for (String fileName : set) {
			QiniuUtils.deleteFileFromQiniu(fileName);
			jedisPool.getResource().srem(RedisConstant.SETMEAL_PIC_RESOURCES, fileName);
			System.out.println("定时任务: 删除垃圾图片,已执行 :  删除图片:" + fileName);
		}
	}

}
