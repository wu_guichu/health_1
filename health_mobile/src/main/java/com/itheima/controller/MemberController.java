package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.itheima.constant.MessageConstant;
import com.itheima.constant.RedisMessageConstant;
import com.itheima.entity.Result;
import com.itheima.pojo.Member;
import com.itheima.service.MemberService;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/member")
public class MemberController {
	@Reference
	private MemberService memberService;
	@Autowired
	private JedisPool jedisPool;
	@RequestMapping("/login")
	public Result login(HttpServletResponse response, @RequestBody Map map) {
		//1.获取手机号和验证码
		String telephone = (String) map.get("telephone");
		String validateCode = (String) map.get("validateCode");
		System.out.println("validateCode ----------------------------------       "+validateCode);
		//2.从redis中获取对应的验证码
		String validateCodeInRedis = jedisPool.getResource().get(telephone + RedisMessageConstant.SENDTYPE_LOGIN);
		System.out.println("validateCodeInRedis ---------------------------       "+validateCodeInRedis);

		//3.验证码对比,若对比成功
		if (validateCode!=null && validateCodeInRedis!=null&&validateCode.equals(validateCodeInRedis)){
			Member member = memberService.findByTelephone(telephone);
			//4.判断是不是会员,若是登录,若不是会员,自动注册
			if(member==null){
				member = new Member();
				member.setRegTime(new Date());
				member.setPhoneNumber(telephone);

				//注册
				memberService.add(member);
			}
			//5.将登录信息保存到cookies中
			Cookie cookie = new Cookie("login_member_telephone", telephone);
			cookie.setPath("/");
			cookie.setMaxAge(60 * 60 * 24 * 30);

			response.addCookie(cookie);
			//6.将会员信息保存到redis中
			String json = JSON.toJSON(member).toString();
			jedisPool.getResource().setex(telephone, 60 * 30, json);

			return new Result(true, MessageConstant.LOGIN_SUCCESS);

		}else {
			//验证码输入错误
			return new Result(false, MessageConstant.VALIDATECODE_ERROR);
		}






	}
}
