package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.Result;
import com.itheima.pojo.Order;
import com.itheima.service.OrderService;
import com.itheima.utils.SMSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

import java.util.Map;

@RestController
@RequestMapping("/order")
public class OrderController {
	@Reference
	private OrderService orderService;

	@Autowired
	private JedisPool jedisPool;

	@RequestMapping("/submit")
	public Result submit(@RequestBody Map map) {
		//获取请求数据中的验证码
		String validateCode = (String)map.get("validateCode");
		//获取请求数据中的电话号
		String telephone = (String)map.get("telephone");
		//从redis中获取电话号码对应的验证码
		String validateCodeInRedis = jedisPool.getResource().get(telephone + SMSUtils.ORDER_NOTICE);
		//将两个验证码对比
		if (validateCode != null && validateCodeInRedis != null && validateCode.equals(validateCodeInRedis)) {
			//如果比对成功，调用服务完成预约业务处理
			map.put("orderType", Order.ORDERTYPE_WEIXIN);//设置预约类型，分为微信预约、电话预约
			Result result = null;
			try{
				result = orderService.order(map);//通过Dubbo远程调用服务实现在线预约业务处理
			}catch (Exception e){
				e.printStackTrace();
				return result;
			}
			if(result.isFlag()){
				//预约成功，可以为用户发送短信
				try{
					SMSUtils.sendShortMessage(SMSUtils.ORDER_NOTICE,telephone, (String) map.get("orderDate"));
				}catch (Exception e){
					e.printStackTrace();
				}
			}
			return result;
		} else {
			//验证码校验失败,
			return new Result(false, MessageConstant.VALIDATECODE_ERROR);
		}
	}
	@RequestMapping("/findById")
	public Result findById(Integer id) {
		try {
			Map map = orderService.findById(id);
			return new Result(true, MessageConstant.QUERY_ORDER_SUCCESS, map);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.QUERY_ORDER_FAIL);
		}
	}

}
