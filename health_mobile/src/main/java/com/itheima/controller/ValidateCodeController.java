package com.itheima.controller;

import com.aliyuncs.exceptions.ClientException;
import com.itheima.constant.MessageConstant;
import com.itheima.constant.RedisMessageConstant;
import com.itheima.entity.Result;
import com.itheima.utils.SMSUtils;
import com.itheima.utils.ValidateCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

@RestController
@RequestMapping("/validateCode")
public class ValidateCodeController {
	@Autowired
	private JedisPool jedisPool;

	//预约体检套餐发送验证码
	@RequestMapping("/send4Order")
	public Result send4Order(String telephone) {
		//生成随机验证码
		Integer validateCode = ValidateCodeUtils.generateValidateCode(4);
		System.out.println(" ----------------------------  "+validateCode);

		try {
			//调用工具类发送验证码
			// SMSUtils.sendShortMessage(SMSUtils.VALIDATE_CODE, telephone, validateCode.toString());
			jedisPool.getResource().setex(telephone + SMSUtils.ORDER_NOTICE, 5 * 60, validateCode.toString());

			return new Result(true, MessageConstant.SEND_VALIDATECODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.SEND_VALIDATECODE_FAIL);
		}
	}

	@RequestMapping("/send4Login")
	public Result send4Login(String telephone) {
		//生成随机验证码
		Integer validateCode4Login = ValidateCodeUtils.generateValidateCode(6);
		System.out.println(validateCode4Login);
		try {
			//将验证码存入redis ----加上登录标志 002
			jedisPool.getResource().setex(telephone + RedisMessageConstant.SENDTYPE_LOGIN, 5 * 60, validateCode4Login.toString());
			return new Result(true, MessageConstant.SEND_VALIDATECODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.SEND_VALIDATECODE_FAIL);
		}

	}
}
