package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckGroup;

import java.util.List;
import java.util.Map;

public interface CheckGroupDao {
	void add(CheckGroup checkGroup);

	void setCheckGroupAndCheckItemId(Map<String, Integer> map);

	Page<CheckGroup> findByCondition(String queryString);

	CheckGroup findById(Integer id);

	List<Integer> findCheckItemIdsByCheckGroupId(Integer id);

	void edit(CheckGroup checkGroup);

	void deleteAssociation(Integer id);

	void delete(Integer id);

	List<CheckGroup> findAll();
}
