package com.itheima.dao;

import com.itheima.pojo.Member;

public interface MemberDao {
	//根据电话号码查询预约信息
	Member findByTelephone(String telephone);
	//添加会员信息
	void add(Member member);

	Integer findMemberCountBeforeDate(String date);
	////查询新增会员数
	Integer findMemberCountByDate(String today);
	//查询总会员数
	Integer findMemberTotalCount();
	//本周新增会员数
	Integer findMemberCountAfterDate(String thisWeekMonday);
}
