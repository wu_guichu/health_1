package com.itheima.dao;

import com.itheima.pojo.Order;

import java.util.List;
import java.util.Map;

public interface OrderDao {
	//
	List<Order> findByCondition(Order order);

	void add(Order order);
	//查询预约信息----预约成功后跳转,页面回显数据
	Map findById4Detail(Integer id);
	//查询今日预约数
	Integer findOrderCountByDate(String today);
	//查询今日到诊数
	Integer findVisitsCountByDate(String today);
	//查询本周预约数
	Integer findOrderCountAfterDate(String thisWeekMonday);
	//查询本周到诊数
	Integer findVisitsCountAfterDate(String thisWeekMonday);
	//查询热门套餐数
	List<Map> findHotSetmeal();


}
