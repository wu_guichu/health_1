package com.itheima.dao;

import com.itheima.pojo.OrderSetting;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface OrderSettingDao {
	Long findCountByOrderDate(Date orderDate);

	void editNumberByOrderDate(OrderSetting orderSetting);

	void add(OrderSetting orderSetting);

	List<OrderSetting> getOrderSettingByMonth(Map<String, String> map);

	//调用dao层,获取当天的预约信息
	OrderSetting findByOrderDate(Date parseString2Date);



	//编辑已预约人数
	void editReservationsByOrderDate(OrderSetting orderSetting);

	//查询数据库,统计日期出现的次数



	//更新,修改预约日期对应的预约数

}
