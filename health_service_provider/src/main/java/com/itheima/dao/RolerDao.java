package com.itheima.dao;

import com.itheima.pojo.Role;

import java.util.Set;

public interface RolerDao {
	//根据角色id查询角色
	Set<Role> findByUserId(Integer id);
}
