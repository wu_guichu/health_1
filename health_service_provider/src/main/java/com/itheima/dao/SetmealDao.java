package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Setmeal;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface SetmealDao {
	void setSetmealAndCheckgroup(HashMap<String, Integer> map);
	void add(Setmeal setmeal);
	Page<Setmeal> findpage(String queryString);

	List<Setmeal> findAll();

	Setmeal findById(Integer id);
	//根据id删除套餐和检查组的多对多关系
	void deleteSetmealAndCheckgroup(Integer id);
	//根据id删除套餐
	void deleteSetmeal(Integer id);
	// 根据检查套餐id查询检查组的id
	List<Integer> findCheckGroupIdsBySetmealId(Integer id);
	//饼图--体检套餐
	List<Map<String, Object>> findSetmealCount();

}
