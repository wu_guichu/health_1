package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.CheckGroupDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;
import com.itheima.service.CheckGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = CheckGroupService.class)
@Transactional
public class CheckGroupServiceImpl implements CheckGroupService {
	@Autowired
	private CheckGroupDao checkGroupDao;
	//新增
	@Override
	public void add(Integer[] checkitemIds, CheckGroup checkGroup) {
		//新增检查组
		checkGroupDao.add(checkGroup);
		//设置检查组和检查项多对多关系
		Integer checkGroupId = checkGroup.getId();
		this.setCheckGroupAndCheckItemId(checkGroupId, checkitemIds);
	}

	@Override
	public PageResult findPage(QueryPageBean queryPageBean) {
		//获取参数
		Integer currentPage = queryPageBean.getCurrentPage();
		Integer pageSize = queryPageBean.getPageSize();
		String queryString = queryPageBean.getQueryString();

		PageHelper.startPage(currentPage, pageSize);
		Page<CheckGroup> page = checkGroupDao.findByCondition(queryString);
		PageResult pageResult = new PageResult(page.getTotal(), page.getResult());
		return pageResult;
	}

	@Override
	public CheckGroup findById(Integer id) {
		CheckGroup checkGroup = checkGroupDao.findById(id);
		return checkGroup;
	}

	@Override
	public List<Integer> findCheckItemIdsByCheckGroupId(Integer id) {
		List<Integer> checkitemIds = checkGroupDao.findCheckItemIdsByCheckGroupId(id);
		return checkitemIds;
	}

	@Override
	public void edit(Integer[] checkitemIds, CheckGroup checkGroup) {
		//修改检查组信息
		checkGroupDao.edit(checkGroup);
		//删除检查项和检查组的中间表--根据id删除
		checkGroupDao.deleteAssociation(checkGroup.getId());
		//重新建立关联
		Integer checkGroupId = checkGroup.getId();
		this.setCheckGroupAndCheckItemId(checkGroupId, checkitemIds);
	}

	@Override
	public void delete(Integer id) {

		checkGroupDao.deleteAssociation(id);

		checkGroupDao.delete(id);

	}

	@Override
	public List<CheckGroup> findAll() {

		return checkGroupDao.findAll();
	}


	//设置检查组和检查项多对多关系
	private void setCheckGroupAndCheckItemId(Integer checkGroupId, Integer[] checkitemIds) {
		if (checkitemIds != null && checkitemIds.length > 0) {
			for (Integer checkitemId : checkitemIds) {
				Map<String,Integer> map =new HashMap<String, Integer>();
				map.put("checkGroupId", checkGroupId);
				map.put("checkitemId", checkitemId);
				checkGroupDao.setCheckGroupAndCheckItemId(map);
			}
		}
	}
}
