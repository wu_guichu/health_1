package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckItemDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = CheckItemService.class)
@Transactional
public class CheckItemServiceImpl implements CheckItemService {
	@Autowired
	private CheckItemDao checkItemDao;

	//添加
	@Override
	public void add(CheckItem checkItem) {
		checkItemDao.add(checkItem);
	}
	//分页查询
	@Override
	public PageResult findPage(QueryPageBean queryPageBean) {
		Integer currentPage = queryPageBean.getCurrentPage();
		Integer pageSize = queryPageBean.getPageSize();
		String queryString = queryPageBean.getQueryString();

		//用插件进行分页查询
		PageHelper.startPage(currentPage, pageSize);
		Page<CheckItem> page = checkItemDao.selectByCondition(queryString);
		long total = page.getTotal();
		List<CheckItem> rows = page.getResult();

		return new PageResult(total,rows);
	}
	//删除
	@Override
	public void deleteById(Integer id) {

		long count = checkItemDao.findCountByCheckItemId(id);

		if (count > 0) {
			throw new RuntimeException();
		}
		checkItemDao.deleteById(id);
	}

	@Override
	public CheckItem findById(Integer id) {
		CheckItem checkItem = checkItemDao.findById(id);
		return checkItem;
	}

	@Override
	public void edit(CheckItem checkItem) {
		checkItemDao.edit(checkItem);
	}

	@Override
	public List<CheckItem> findAll() {
		checkItemDao.findAll();
		return checkItemDao.findAll();
	}


}
