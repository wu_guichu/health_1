package com.itheima.service.impl;


import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.constant.MessageConstant;
import com.itheima.dao.MemberDao;
import com.itheima.dao.OrderDao;
import com.itheima.dao.OrderSettingDao;
import com.itheima.entity.Result;
import com.itheima.pojo.Member;
import com.itheima.pojo.Order;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderService;
import com.itheima.utils.DateUtils;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = OrderService.class)
@Transactional
public class OrderServiceImpl implements OrderService {

	@Autowired//远程注入
	private OrderSettingDao orderSettingDao;
	@Autowired
	private MemberDao memberDao;
	@Autowired
	private OrderDao orderDao;
	//体检预约提交
	@Override
	public Result order(Map map) throws Exception {
		//从map集合中获取date对象
		String orderDate = (String) map.get("orderDate");
		//调用dao层,获取当天的预约信息 ------------------------
		OrderSetting orderSetting = orderSettingDao.findByOrderDate(DateUtils.parseString2Date(orderDate));

		//1.判定是否设置了预约信息
		if (orderSetting == null) {
			//若预约信息未设置,则不能预约
			return new Result(false, MessageConstant.SELECTED_DATE_CANNOT_ORDER);
		}
		//2.判定是否预约满了
		if (orderSetting.getReservations() >= orderSetting.getNumber()) {
			return new Result(false, MessageConstant.ORDER_FULL);
		}
		//3.判定是否重复预约: 同一天,同一个用户,同一个套餐只能预约一次
		String telephone = (String) map.get("telephone");
		Member member = memberDao.findByTelephone("telephone"); //  ---------------------------------
		if (member != null) {
			Integer memberId = member.getId();//获取id
			Date order_Date = DateUtils.parseString2Date(orderDate);//数据库中
			String setmealId = (String) map.get("setmealId");//套餐id
			Order order = new Order(memberId, order_Date, Integer.parseInt(setmealId));
			//查询预约是否重复
			List<Order> list = orderDao.findByCondition(order);  //----------------------------

			if (list != null && list.size() > 0) {
				//不能重复预约
				return new Result(false, MessageConstant.HAS_ORDERED);
			}

		}else {
			//4.判定当前预约用户是否为会员,若不是,则自动注册
			member = new Member();
			member.setName((String) map.get("name"));
			member.setPhoneNumber(telephone);
			member.setIdCard((String) map.get("idCard"));
			member.setRegTime(new Date());
			memberDao.add(member);//完成注册 -----------------
		}
		//5.记录预约信息,更新预约人数
		Order order = new Order();
		order.setMemberId(member.getId());//设置会员id
		order.setOrderDate(DateUtils.parseString2Date(orderDate));//设置预约日期
		order.setOrderType((String) map.get("orderType"));//设置预约类型
		order.setOrderStatus(Order.ORDERSTATUS_NO);//设置到诊状态
		order.setSetmealId(Integer.parseInt((String) map.get("setmealId")));//设置预约套餐的id

		orderDao.add(order);

		//设置已预约人数----人数加一
		orderSetting.setReservations(orderSetting.getReservations() + 1);//设置已预约人数+1

		orderSettingDao.editReservationsByOrderDate(orderSetting); // --------------

		return new Result(true,MessageConstant.ORDER_SUCCESS,order.getId());

	}

	//查询预约信息----预约成功后跳转,页面回显数据
	@Override
	public Map findById(Integer id)throws Exception {

		Map map = orderDao.findById4Detail(id);
		if (map != null) {
			Date orderDate = (Date) map.get("orderDate");
			map.put("orderDate", DateUtils.parseDate2String(orderDate));
		}

		return map;
	}

}
