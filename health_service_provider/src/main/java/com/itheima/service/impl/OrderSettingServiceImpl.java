package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.OrderSettingDao;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service(interfaceClass = OrderSettingService.class)
@Transactional
public class OrderSettingServiceImpl implements OrderSettingService {
	@Autowired
	private OrderSettingDao orderSettingDao;



	/**
	 * 判断所加的信息,日期是否存在
	 * 1.查询数据库的日期信息
	 * 2.若日期不存在,则添加
	 * 3.若日期已存在,则更新
	 * @param
	 */
	@Override
	public void add(List<OrderSetting> list) {
		//非空判断
		if (list != null && list.size() > 0) {

			for (OrderSetting orderSetting : list) {
				//调用service查找日期对应的记录
				Long countByOrderDate = orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());
				if (countByOrderDate > 0) {
					orderSettingDao.editNumberByOrderDate(orderSetting);
				} else {
					orderSettingDao.add(orderSetting);
				}


			}

		}
	}






	/**
	 * 日历展示预约信息
	 * @param date
	 * @return
	 */
	@Override
	public List<Map> getOrderSettingByMonth(String date) {
		String begin = date + "-1";
		String end = date + "-31";
		Map<String, String> map = new HashMap<>();
		map.put("begin", begin);
		map.put("end", end);

		List<OrderSetting> list = orderSettingDao.getOrderSettingByMonth(map);

		List<Map> result = new ArrayList<>();
		if (list != null && list.size() > 0) {
			for (OrderSetting orderSetting : list) {
				Map<String, Object> mm = new HashMap<>();
				mm.put("date", orderSetting.getOrderDate().getDate());
				mm.put("number", orderSetting.getNumber());
				mm.put("reservations", orderSetting.getReservations());
				result.add(mm);
			}
		}
		return result;
	}

	@Override
	public void editNumberByDate(OrderSetting orderSetting) {
		Long count = orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());
		if (count > 0) {
			orderSettingDao.editNumberByOrderDate(orderSetting);
		} else {
			orderSettingDao.add(orderSetting);
		}
	}

}
