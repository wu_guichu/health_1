package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.constant.RedisConstant;
import com.itheima.dao.SetmealDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Setmeal;
import com.itheima.service.SetmealService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfig;
import redis.clients.jedis.JedisPool;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = SetmealService.class)
@Transactional
public class SetmealServiceImpl implements SetmealService {

	@Autowired
	private JedisPool jedisPool;
	@Autowired
	private SetmealDao setmealDao;
	@Override
	public void add(Setmeal setmeal, Integer[] checkgroupIds) {
		setmealDao.add(setmeal);
		Integer setmealId = setmeal.getId();
		this.setSetmealAndCheckgroup(setmealId, checkgroupIds);

		//将图片名称保存到redis中
		String fileName = setmeal.getImg();
		jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_DB_RESOURCES, fileName);
		//添加套餐后重新生成静态页面----所有页面全部重新生成
		// generateMobileStaticHtml();

		//添加体检套餐后,生成新添加的体检套餐的静态页面,
		//添加体检套餐后,生成新添加的体检套餐的静态页面,  add
		generateMobileStaticHtml(setmealId);
	}
	//首次生成静态页面--生成套餐页面和套餐详情
	public void generateMobileStaticHtml() {
		//1.查询信息
		List<Setmeal> list = setmealDao.findAll();
		//2.生成套餐列表静态页面
		generateMobileSetmealListHtml(list);
		//3.生成套餐详情静态页面
		generateMobileSetmealDetailHtml(list);
	}
	//删除单个套餐后生成静态页面
	public void generateMobileStaticHtmlAfterDelete() {
		//1.查询信息
		List<Setmeal> list = setmealDao.findAll();
		//2.生成套餐列表静态页面
		generateMobileSetmealListHtml(list);

	}

	//添加体检套餐后,生成套餐列表静态页面,生成新添加的套餐详情的静态页面  add
	//添加体检套餐后,生成套餐列表静态页面,生成新添加的套餐详情的静态页面  add
	//添加体检套餐后,生成套餐列表静态页面,生成新添加的套餐详情的静态页面  add
	public void generateMobileStaticHtml(Integer setmealId) {
		//1.查询信息
		List<Setmeal> list = setmealDao.findAll();
		//2.生成套餐列表静态页面
		generateMobileSetmealListHtml(list);
		//3.生成新添加的套餐详情静态页面
		generateMobileSetmealDetailHtml(setmealId);
	}
	// 生成套餐列表静态页面  生成套餐列表,每次发生改变都会调用
	public void generateMobileSetmealListHtml(List<Setmeal> list) {
		Map map = new HashMap();
		map.put("setmealList", list);
		generteHtml("mobile_setmeal.ftl", "m_setmeal.html", map);
	}
	//  生成所有套餐详情静态页面
	public void generateMobileSetmealDetailHtml(List<Setmeal> list) {
		for (Setmeal setmeal : list) {
			Map map = new HashMap();
			map.put("setmeal", setmealDao.findById(setmeal.getId()));
			generteHtml("mobile_setmeal_detail.ftl", "setmeal_detail_" + setmeal.getId() + ".html", map);
		}
	}
	//  生成新添加的套餐详情静态页面 add
	//  生成新添加的套餐详情静态页面 add
	//  生成新添加的套餐详情静态页面 add
	public void generateMobileSetmealDetailHtml(Integer setmealId) {

		Map map = new HashMap();
		map.put("setmeal", setmealDao.findById(setmealId));
		generteHtml("mobile_setmeal_detail.ftl", "setmeal_detail_" + setmealId + ".html", map);

	}

	@Autowired
	private FreeMarkerConfig freeMarkerConfig;
	@Value("${out_put_path}")
	private String outPutPath;
	//  生成静态页面
	public void generteHtml(String templateName, String htmlPageName, Map map) {
		Configuration configuration = freeMarkerConfig.getConfiguration();
		configuration.setDefaultEncoding("UTF-8");
		Writer out = null;
		try {

			Template template = configuration.getTemplate(templateName);
			//构造输出流
			out = new FileWriter(new File(outPutPath + "/" + htmlPageName));
			//输出
			template.process(map, out);
			//关闭流
			out.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	//分页查询
	@Override
	public PageResult pageQuery(QueryPageBean queryPageBean) {
		Integer currentPage = queryPageBean.getCurrentPage();
		Integer pageSize = queryPageBean.getPageSize();
		String queryString = queryPageBean.getQueryString();

		PageHelper.startPage(currentPage, pageSize);
		Page<Setmeal> page = setmealDao.findpage(queryString);

		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public List<Setmeal> findAll() {
		List<Setmeal> list = setmealDao.findAll();

		return list;
	}

	@Override
	public Setmeal findById(Integer id) {
		return setmealDao.findById(id);
	}
	//删除体检套餐信息
	@Override
	public void delete(Integer id) {
		//根据id删除套餐和检查组的多对多关系
		setmealDao.deleteSetmealAndCheckgroup(id);
		//根据id删除套餐
		setmealDao.deleteSetmeal(id);
		//根据id删除套餐后,重新生成套餐详情页面
		generateMobileStaticHtmlAfterDelete();
	}

	//根据检查套餐id查询检查组的id
	@Override
	public List<Integer> findCheckGroupIdsBySetmealId(Integer id) {
		return setmealDao.findCheckGroupIdsBySetmealId(id);
	}
	//编辑 -----先删除,再调用添加
	@Override
	public void edit(Integer[] checkgroupIds,Setmeal setmeal) {
		//根据id删除套餐和检查组的多对多关系
		setmealDao.deleteSetmealAndCheckgroup(setmeal.getId());
		//根据id删除套餐
		setmealDao.deleteSetmeal(setmeal.getId());

		//调用添加方法添加
		this.add(setmeal, checkgroupIds);
	}

	//饼图--查询体检套餐
	@Override
	public List<Map<String, Object>> findSetmealCount() {
		return setmealDao.findSetmealCount();
	}

	//设置套餐和检查组的多对多表关系
	public void setSetmealAndCheckgroup(Integer setmealId, Integer[] checkgroupIds) {
		if (checkgroupIds != null && checkgroupIds.length > 0) {
			for (Integer checkgroupId : checkgroupIds) {
				HashMap<String, Integer> map = new HashMap<>();
				map.put("setmealId", setmealId);
				map.put("checkgroupId", checkgroupId);
				setmealDao.setSetmealAndCheckgroup(map);
			}
		}
	}


}
