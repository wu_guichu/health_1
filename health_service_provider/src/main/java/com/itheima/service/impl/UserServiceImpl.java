package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.PermissionDao;
import com.itheima.dao.RolerDao;
import com.itheima.dao.UserDao;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.pojo.User;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service(interfaceClass = UserService.class)
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;
	@Autowired
	private RolerDao rolerDao;
	@Autowired
	private PermissionDao permissionDao;
	@Override
	public User findByUsername(String username) {
		//获取user,仅有user数据
		User user = userDao.findBuUsername(username);
		if (user == null) {
			return null;
		}

		Integer userId = user.getId();
		//获取id对应的Role和permission
		//根据id查询role
		Set<Role> roles = rolerDao.findByUserId(userId);
		for (Role role : roles) {
			Integer roleId = role.getId();

			Set<Permission> permissions = permissionDao.findByRoleId(roleId);
			role.setPermissions(permissions);
		}
		//根究id查询permission
		user.setRoles(roles);

		return user;
	}
}
